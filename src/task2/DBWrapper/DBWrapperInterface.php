<?php

namespace Src\task2\DBWrapper;

interface DBWrapperInterface
{
    public function setConfig($config);
    public function insertCollection($collection);
    public function addDeferredInsertEntity($entity);
    public function deferredInserting();

}
