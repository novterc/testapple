<?php

namespace Src\task2\DBWrapper;

abstract class DBWrapper implements DBWrapperInterface
{
    protected $config;
    protected $deferredInserts;

    abstract protected function insertCollectionByClass($class, $entities);

    public function setConfig($config)
    {
        $this->config = $config;
    }

    protected function getConfig($name)
    {
        return $this->config[$name];
    }

    protected function sortEntityByClass($entityCollection)
    {
        $insertList = [];
        foreach ($entityCollection as $entity) {
            $insertList[get_class($entity)][] = $entity;
        }

        return $insertList;
    }

    protected function getFieldsByClass($class)
    {
        return array_keys(get_class_vars($class));
    }

    public function insertCollection($entityCollection)
    {
        $insertListByClass = $this->sortEntityByClass($entityCollection);
        foreach ($insertListByClass as $class => $entities) {
            $this->insertCollectionByClass($class, $entities);
        }
    }

    public function addDeferredInsertEntity($entity)
    {
        $this->deferredInserts[] = $entity;
    }

    public function deferredInserting()
    {
        $this->insertCollection($this->deferredInserts);
        $this->deferredInserts = [];
    }

}
