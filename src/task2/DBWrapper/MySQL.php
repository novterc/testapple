<?php

namespace Src\task2\DBWrapper;

class MySQL extends DBWrapper
{
    protected $pdo;

    protected function connect()
    {
        $this->pdo = new \PDO(
            "mysql:host={$this->getConfig('host')};dbname={$this->getConfig('dbname')}",
            $this->getConfig('username'),
            $this->getConfig('password')
        );
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    protected function getPDO()
    {
        if ($this->pdo === null) {
            $this->connect();
        }

        return $this->pdo;
    }

    protected function insertCollectionByClass($class, $entities)
    {
        $fields = $this->getFieldsByClass($class);
        $fieldsStr = $this->getFieldByClassStrByArray($fields);
        $tableName = $class::TABLENAME;
        $insertValuesMap = $this->getInsertValuesMapStr(count($entities), count($fields));
        $queryStr = "INSERT INTO `{$tableName}` ({$fieldsStr}) VALUES ".$insertValuesMap;

        $stmt = $this->getPDO()->prepare($queryStr);

        $this->setInsertValueByStmtAndEntities($stmt, $class, $entities);
        $stmt->execute();
    }

    protected function getFieldByClassStrByArray($fields)
    {
        $fieldsWrap = array_map(
            function ($item) {
                return "`{$item}`";
            },
            $fields
        );

        return implode(',', $fieldsWrap);
    }

    protected function getInsertValuesMapStr($rowCount, $valuesCount)
    {
        $rowArr = [];
        for ($i = 0; $i < $rowCount; $i++) {
            $rowArr[] = $this->getInsertValuesMapRow($valuesCount);
        }

        return implode(',', $rowArr);
    }

    protected function getInsertValuesMapRow($count)
    {
        $valuesMapArr = array_fill(0, $count, "?");
        $valuesMapStr = implode(',', $valuesMapArr);

        return '('.$valuesMapStr.')';
    }

    protected function setInsertValueByStmtAndEntities($stmt, $class, $entities)
    {
        $fields = $this->getFieldsByClass($class);
        $i = 1;
        foreach ($entities as $entity) {
            foreach ($fields as $field) {
                $value = $entity->$field;
                $stmt->bindValue($i, $value);
                $i++;
            }
        }
    }

}
