<?php

namespace Src\task2\Parsers;

class CSVParser implements ParserInterface
{
    const FIELDSNAME = [];
    const DELIMITER = ';';
    const ENCLOSURE = '"';
    const ESCAPECHAR = '\\';

    protected $filePathName;
    protected $splFileObj;

    public function setFile($filePathName)
    {
        $this->filePathName = $filePathName;
        $this->splFileObj = $this->initSplFileObj($filePathName);
    }

    protected function initSplFileObj($filePathName)
    {
        return new \SplFileObject($filePathName);
    }

    protected function getDataFieldName($data)
    {
        $newData = [];
        foreach ($data as $key => $value) {
            if(empty(static::FIELDSNAME[$key])) {
                continue;
            }
            $newData[static::FIELDSNAME[$key]] = $value;
        }

        return $newData;
    }

    public function current()
    {
        $data = $this->splFileObj->fgetcsv(static::DELIMITER, static::ENCLOSURE, static::ESCAPECHAR);
        $data = $this->getDataFieldName($data);

        return $data;
    }

    public function next()
    {
        return $this->splFileObj->next();
    }

    public function key()
    {
        return $this->splFileObj->key();
    }

    public function valid()
    {
        return $this->splFileObj->valid();
    }

    public function rewind()
    {
        return $this->splFileObj->rewind();
    }

}
