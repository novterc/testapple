<?php

namespace Src\task2\Loaders;

class LoaderType1 extends Loader
{
    public function __construct()
    {
        $this->setParser(new \Src\task2\Parsers\CSVType1());
        $this->setEntityClass(new \Src\task2\Entities\Log1());
        $this->setDBWrapper(new \Src\task2\DBWrapper\MySQL());
    }
}
