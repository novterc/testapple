<?php

namespace Src\task2\Loaders;

class LoaderType2 extends Loader
{
    public function __construct()
    {
        $this->setParser(new \Src\task2\Parsers\CSVType2());
        $this->setEntityClass(new \Src\task2\Entities\Log2());
        $this->setDBWrapper(new \Src\task2\DBWrapper\MySQL());
    }
}
