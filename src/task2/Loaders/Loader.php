<?php

namespace Src\task2\Loaders;

use Src\task2\DBWrapper\DBWrapperInterface;
use Src\task2\Entities\EntitiInterface;
use Src\task2\Parsers\CSVType1;

abstract class Loader implements LoaderInerface
{
    const LOADERDBLIMIT = 1000;

    public $parser;
    public $entityClass;
    public $dbWrapper;

    protected function setParser($parser)
    {
        $this->parser = $parser;
    }

    protected function setEntityClass(EntitiInterface $entityClass)
    {
        $this->entityClass = $entityClass;
    }

    protected function setDBWrapper(DBWrapperInterface $dbWrapper)
    {
        $this->dbWrapper = $dbWrapper;
    }

    protected function getEntity()
    {
        return new $this->entityClass();
    }

    protected function setDataInEntity($entity, $data)
    {
        foreach ($data as $name => $value) {
            if (!property_exists($this->entityClass, $name)) {
                throw new \Exception('Not exist property - '.$name);
            }
            $entity->$name = $value;
        }

        return $entity;
    }

    public function loading()
    {
        $limit = 0;
        foreach ($this->parser as $dataItem) {
            $entity = $this->getEntity();
            $entity = $this->setDataInEntity($entity, $dataItem);
            $this->dbWrapper->addDeferredInsertEntity($entity);
            $limit++;
            if ($limit === static::LOADERDBLIMIT) {
                $this->dbWrapper->deferredInserting();
                $limit = 0;
            }
        }
        $this->dbWrapper->deferredInserting();
    }
}
