<?php

namespace Src\task2\Entities;

class Log2 implements EntitiInterface
{
    const TABLENAME = 'log2';

    public $ip_address;
    public $browser;
    public $os;
}
