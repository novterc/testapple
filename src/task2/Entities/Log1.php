<?php

namespace Src\task2\Entities;

class Log1 implements EntitiInterface
{
    const TABLENAME = 'log1';

    public $date_time;
    public $ip_address;
    public $url_referer;
    public $url;
}
