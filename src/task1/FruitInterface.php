<?php

namespace Src\task1;

interface FruitInterface
{
    public function fallToGround();

    public function eat($percent);

    public static function lostHour();
}
