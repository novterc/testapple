<?php

namespace Src\task1;

abstract class Fruit implements FruitInterface
{
    const LIFETIME = 0;

    public static $itemList = [];
    protected $createTime;
    protected $lifeTimePassed = 0;
    protected $failTime;
    protected $isFail = false;
    public $isCorrupted = false;
    public $color;
    public $size = 1;

    public function __construct($color)
    {
        $this->color = $color;
        $this->createTime = time();
        static::$itemList[] = $this;
    }

    /**
     * Set percent eat
     *
     * @param integer $percent
     * @return bool
     * @throws \Exception
     */
    public function eat($percent)
    {
        if (!is_numeric($percent) || $percent < 0) {
            throw new \Exception('invalid value');
        }

        if (!$this->isFail || $this->isCorrupted || $this->size === 0) {
            return false;
        }

        $this->size = round($this->size - 0.01 * $percent, 2);
        if ($this->size <= 0) {
            $this->size = 0;
            static::removeItem($this);
        }

        return true;
    }

    /**
     * Remove item from itemList by instance
     *
     * @param object $instance
     * @return bool
     */
    protected static function removeItem($instance)
    {
        foreach (static::$itemList as $itemKey => $checkItem) {
            if ($checkItem === $instance) {
                unset(static::$itemList[$itemKey]);

                return true;
            }
        }

        return false;
    }

    /**
     * Set fail and failTime
     */
    public function fallToGround()
    {
        $this->isFail = true;
        $this->failTime = $this->createTime + $this->lifeTimePassed;
    }

    /**
     * @param integer $time
     * @throws \Exception
     */
    protected function setLifeTimePassed($time)
    {
        if (!is_numeric($time) || $time < 0) {
            throw new \Exception('invalid value');
        }

        $this->lifeTimePassed += $time;
        $this->checkAndSetCorrupted();
    }

    /**
     * Checks time after fall and sets isCorrupted
     *
     * @return bool
     */
    protected function checkAndSetCorrupted()
    {
        if ($this->isFail && !$this->isCorrupted) {
            if ($this->getTimeAfterFail() >= static::LIFETIME) {
                $this->setCorrupted();

                return true;
            }
        }

        return false;
    }

    /**
     * Get time after fail
     *
     * @return int
     */
    public function getTimeAfterFail()
    {
        return $this->lifeTimePassed - ($this->failTime - $this->createTime);
    }

    /**
     *  Set corrupted
     */
    protected function setCorrupted()
    {
        $this->isCorrupted = true;
    }

    /**
     * Passes through all instances of the class and adds passed time
     */
    public static function lostHour()
    {
        $lostTime = 60 * 60;
        foreach (static::$itemList as $item) {
            $item->setLifeTimePassed($lostTime);
        }
    }

}
