
CREATE TABLE `log1` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`date_time` DATETIME NOT NULL,
	`ip_address` VARCHAR(50) NOT NULL,
	`url_referer` VARCHAR(250) NOT NULL,
	`url` VARCHAR(250) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `ip_address` (`ip_address`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=8
;


CREATE TABLE `log2` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`ip_address` VARCHAR(50) NULL DEFAULT NULL,
	`browser` VARCHAR(50) NULL DEFAULT NULL,
	`os` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `ip_address` (`ip_address`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=5
;
