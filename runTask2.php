<?php

include __DIR__ . '/vendor/autoload.php';

use Src\task2\Loaders\LoaderType1;
use Src\task2\Loaders\LoaderType2;

$mysqlConfig = [
    'host' => 'localhost',
    'dbname' => 'test',
    'username' => 'root',
    'password' => 'passwordd',
];

$loader1 = new LoaderType1();
$loader1->parser->setFile(__DIR__ . '/resources/type_1.csv');
$loader1->dbWrapper->setConfig($mysqlConfig);
$loader1->loading();

$loader2 = new LoaderType2();
$loader2->parser->setFile(__DIR__ . '/resources/type_2.csv');
$loader2->dbWrapper->setConfig($mysqlConfig);
$loader2->loading();
